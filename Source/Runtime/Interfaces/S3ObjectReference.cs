using System;

using Manganese.CoreKit;


namespace Manganese.AwsKit {
  public class S3ObjectReference{
    public string region;
    public string bucket;
    public string path;

    public string urlString {
      get {
        return "https://s3." + this.region + ".amazonaws.com/" + this.bucket + "/" + this.path;
      }
    }

    public Uri uri {
      get {
        return new Uri(this.urlString);
      }
    }

    public Link ToLink() {
      return new Link(this.uri);
    }
  }
}